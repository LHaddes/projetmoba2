﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/**
 * Creep
 */
public class Creep : MonoBehaviour
{
    public int life;
    public int damage;

    // Gestion déplacement
    private NavMeshAgent _agent;
    private Transform _moveTarget;
    public Transform finishDirection;

    // Gestion détection
    public List<GameObject> currentHitObjects = new List<GameObject>();
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;
    private Vector3 originCreep;
    private Vector3 directionCreep;

    private float currentHitDistance;

    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            // Detruire le creep
            Destroy(this);
            // Donne + de gold
        }

        // Initialisation des variables
        originCreep = transform.position;
        directionCreep = transform.forward;

        currentHitObjects.Clear();
        RaycastHit[] hits = Physics.SphereCastAll(originCreep, sphereRadius, directionCreep, maxDistance, layerMask,
            QueryTriggerInteraction.UseGlobal);
        Debug.Log("La tour détecte " + hits.Length + " élément(s)");

        // Parcrours des RaycastHit
        foreach (RaycastHit hit in hits)
        {
            currentHitObjects.Add(hit.transform.gameObject);
            currentHitDistance = hit.distance;
        }
        _agent.SetDestination(finishDirection.position);
        
        Attack();

        
    }

    private void Attack()
    {
        foreach (GameObject hitObject in currentHitObjects)
        {
            if (hitObject.layer.Equals(LayerMask.NameToLayer("Tower")))
            {
                Debug.Log("Attaque !");
                // Redirection vers la toure
                _agent.SetDestination(hitObject.transform.position);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Tower")
            {
                Debug.Log("Collision Tower");
                Tower tower = other.GetComponent<Tower>();
                if (tower != null)
                {
                    tower.GetDamage(damage);
                }
                else
                {
                    Debug.Log("Problème objet toure !");
                }
                
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            //Debug.DrawLine(originCreep, originCreep + directionCreep * currentHitDistance);
            Gizmos.DrawWireSphere(originCreep + directionCreep * currentHitDistance, sphereRadius);
        }
    }