﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Vianney
 */
public class SpawnerCreep : MonoBehaviour
{
    public GameObject creepPrefabs;
    public int nbSpawn;
    public float timerSpawn = 0f;
    
    // Temps entre chaque spawn de creep
    public float timeBetweenSpawn = 10f;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (timerSpawn <= 0f)
        {
            InvokeRepeating("SpawnVert", 0f, 0.5f);
        }
        else
        {
            timerSpawn -= Time.deltaTime;
        }
        
        if (nbSpawn >= 4)
        {
            CancelInvoke("SpawnVert");
            if (timerSpawn <= 0f)
            {
                // Réinitialisation du timer
                timerSpawn = 10f;
                // Réinitialisation du nb de spawn
                nbSpawn = 0;
            }
            
        }
    }
    
    public void SpawnVert()
    {
        if (timeBetweenSpawn <= 0f)
        {
            Instantiate(creepPrefabs, transform.position,Quaternion.identity);
            nbSpawn += 1;
            timeBetweenSpawn = 10f;
        }
        else
        {
            timeBetweenSpawn -= Time.deltaTime;
        }
        
    }
}
