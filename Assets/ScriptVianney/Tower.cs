﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public int lifeTower;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void GetDamage(int damage)
    {
        lifeTower -= damage;
    }

    // Update is called once per frame
    void Update()
    {
        // Destruction toure
        if (lifeTower <= 0)
        {
            Destroy(this);
        }
    }
}
