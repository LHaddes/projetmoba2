﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Vianney
 */
public class DetectionCreeps : MonoBehaviour
{
    public List<GameObject> currentHitObjects = new List<GameObject>();
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;
    private Vector3 originTower;
    private Vector3 directionTower;

    private float currentHitDistance;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Initialisation des variables
        originTower = transform.position;
        directionTower = transform.forward;

        currentHitObjects.Clear();
        RaycastHit[] hits = Physics.SphereCastAll(originTower, sphereRadius, directionTower, maxDistance, layerMask,
            QueryTriggerInteraction.UseGlobal);
        Debug.Log("La tour détecte " + hits.Length + " élément(s)");

        // Parcrours des RaycastHit
        foreach (RaycastHit hit in hits)
        {
            currentHitObjects.Add(hit.transform.gameObject);
            currentHitDistance = hit.distance;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(originTower, originTower + directionTower * currentHitDistance);
        Gizmos.DrawWireSphere(originTower + directionTower * currentHitDistance, sphereRadius);
    }
}