﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstreNeutre : MonoBehaviour
{
    public List<GameObject> currentHitObjects = new List<GameObject>(); // list des objets 
    public float sphereRadius;       //radius autour du monstre
    public float maxDistance;        
    public LayerMask layerMask;
    private Vector3 originTower;      
    private Vector3 directionTower;
    public int hpMaxMonster, hpMonster;  //vie de l'ennemis

    private float currentHitDistance;
    public GameObject targ;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Initialisation des variables
        originTower = transform.position;   
        directionTower = transform.forward; 

        currentHitObjects.Clear();
        RaycastHit[] hits = Physics.SphereCastAll(originTower, sphereRadius, directionTower, maxDistance, layerMask,
            QueryTriggerInteraction.UseGlobal);                                 //permet de detecter les gens autours
        
        foreach (RaycastHit hit in hits)
        {
            currentHitObjects.Add(hit.transform.gameObject);  //detecte tout les ennemis et les mets dans une list
            currentHitDistance = hit.distance;
        }

        targ = currentHitObjects[0];    //le target sera le premier joueur de la list 

        if (hpMonster < hpMaxMonster) //si la vie du monstre et en dessous de son max / il a ete blesser 
        {
            //alors il va suivre son target (donc le blesser si il y a des collision)
            transform.position = Vector3.MoveTowards(transform.position, targ.transform.position, 1f);
        }
    }

    private void OnDrawGizmosSelected()  //pour voir le raycast
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(originTower, originTower + directionTower * currentHitDistance);
        Gizmos.DrawWireSphere(originTower + directionTower * currentHitDistance, sphereRadius);
    }
}
