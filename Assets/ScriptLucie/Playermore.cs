﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Playermore : MonoBehaviour
{
    public int hpTest, manaTest, moneyTest , manaMaxTest, hpMaxTest , itemValue;
    public TextMeshProUGUI hpText,manaText,moneyText;
    public GameObject shopUI ,item1 ,item2 , item3 , item4;
    public Button button1, button2, button3, button4;
    void Start()
    {
        
    }

    void Update()
    {
        hpText.text = hpTest.ToString() + "/" + hpMaxTest.ToString();          //PV    TA VIE / TA VIE MAX
        manaText.text = manaTest.ToString() +"/" + manaMaxTest.ToString();     // MANA   TON MANA / TON MANA MAX
        moneyText.text = moneyTest.ToString() + "$";                           //TON ARGENT
    }

    public void Shop()
    {
        shopUI.SetActive(true);               // OUVRIR LE SHOP
    }

    public void Achat(int valeur)
    {
        if (moneyTest >= valeur)
        {
            itemValue = valeur;                       // l'objet en question 
            moneyTest = moneyTest - valeur;           //TU PERDS LA VALEUR DE L OBJECT QUE TU AS ACHETER
        }

        if (itemValue == 45)                    //si l'objet en question est l'objet 1
        {
            item1.SetActive(true);                      //il apparrait dans l'inventaire
            // attaque + 50;                            // il boost de 50 l'attaque 
            button1.interactable = (false);            //tu ne peux plus l'acheter
        }
        else if (itemValue == 30)                //si l'objet en question est l'objet 2
        {
            item2.SetActive(true);                   //il apparrait dans l'inventaire
            manaTest = manaTest + 50;
            manaMaxTest = manaMaxTest +50;           // le mana augmente en globalité de 50
            button2.interactable = (false);           //tu ne peux plus l'acheter
        }
        else if (itemValue == 75)
        {
            item3.SetActive(true);                        //il apparrait dans l'inventaire
            hpTest = hpTest + 50;                         // la vie augmente en globalité de 50
            hpMaxTest = hpMaxTest + 50;
            button3.interactable = (false);                //tu ne peux plus l'acheter
        }
        else if (itemValue == 100)
        {
            item4.SetActive(true);                       //apparrait 
            //faire apparraitre le spawn 5sec           //spawner Pendant 5s
            button4.interactable = (false);             //tu ne peux plus l'acheter
        }
    }

    public void Close()
    {
        shopUI.SetActive(false);          //FERMER LE SHOP
    }
}
