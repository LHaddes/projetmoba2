﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Stats stats;
    private Mort _mort;

    public bool isDead = false;
    
    
    void Start()
    {
        stats.pv = stats.pvMax;
        stats.mana = stats.manaMax;
    }

    
    void Update()
    {
        if (!isDead)
        {
            stats.pv += 0.002f;
            stats.mana += 0.002f;

            if (stats.pv >= stats.pvMax)
            {
                stats.pv = stats.pvMax;
            }
            if (stats.mana >= stats.manaMax)
            {
                stats.mana = stats.manaMax;
            }
        }
    }
}
