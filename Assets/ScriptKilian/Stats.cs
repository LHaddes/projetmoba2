﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats
{
    public string nomDuChampion;

    public float pv,
        mana,
        niveau,
        exp;
        
    public int attackDamage,
        abilityPower,
        armor,
        magicResistance,
        movementSpeed,
        attackSpeed,
        criticalChance;

    public float pvMax, 
        manaMax;
    
}
