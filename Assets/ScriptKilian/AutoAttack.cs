﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttack : MonoBehaviour
{
    public GameObject bullet;
    public float cooldown, cooldownBase;

    public LayerMask layerEnnemi;
    public float radiusSphere = 3f;
    
    public Vector3 playerPosition;
    
    void Update()
    {
        playerPosition = transform.position;
        RaycastHit hit;
        
        if (Physics.SphereCast(transform.position, radiusSphere, Vector3.forward, out hit, 0f, layerEnnemi))
        {
            Debug.Log("test");
            
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Ennemi" && Input.GetMouseButtonDown(1))
        {
            Instantiate(bullet, transform.position, transform.rotation);
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        
        Gizmos.DrawWireSphere(transform.position + Vector3.forward, radiusSphere);
    }
}
