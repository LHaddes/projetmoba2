﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capacite1 : MonoBehaviour
{
    public string nomSort;
    public int coutEnMana, degats;
    
    public GameObject zoneHitbox;

    public float cooldown, cooldownBase;

    private Vector3 _mousePosition;
    public bool _isCasting;

    void Start()
    {
        cooldown = cooldownBase;
    }

    void Update()
    {
        _mousePosition = Input.mousePosition;
        
        if (Input.GetButtonDown("CastSpell") && !_isCasting)
        {
            Debug.Log("Cast");
            _isCasting = true;
            Instantiate(zoneHitbox, _mousePosition, transform.rotation);
        }

    }
    
}
