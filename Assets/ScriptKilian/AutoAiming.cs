﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAiming : MonoBehaviour
{
    public Transform target;
    void Update()
    {
        target = GameObject.FindWithTag("Ennemi").transform;
        transform.LookAt(target);
        transform.Translate(Vector3.forward * Time.deltaTime * 1.5f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ennemi")
        {
            Destroy(this);
            Debug.Log("Ennemi");
        }
    }
}
